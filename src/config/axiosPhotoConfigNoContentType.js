import axios from "axios";

const api = axios.create({
  baseURL: process.env.VUE_APP_POHTO_URL || "http://localhost:8088"
});

api.interceptors.request.use((config) => {
  const token = sessionStorage.getItem("Authorization"); 
  config.headers["Authorization"] = `Bearer ${token}`;
  console.log("config")
  console.log(config);
  return config;
});

export default api;
