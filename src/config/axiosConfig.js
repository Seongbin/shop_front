import axios from "axios";

const api = axios.create({
  baseURL: process.env.VUE_APP_BASEURL || "http://localhost:8085"
});

api.interceptors.request.use((config) => {
  const token = sessionStorage.getItem("Authorization");
  // config.headers["Accept-Language"] = sessionStorage["language"] 
  config.headers["Authorization"] = `Bearer ${token}`;
  console.log("config")
  console.log(config);
  return config;
});

export default api;
