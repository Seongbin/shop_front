import Vue from "vue";
import VueRouter from "vue-router";

import PageHome from "@/components/pageHome.vue";
import Login from "@/components/user/login.vue";
import Register from "@/components/user/register.vue";
import ChangeUserInfo from "@/components/user/changeUserInfo.vue";

import Product from "@/components/product/product.vue";

import CreateComment from "@/components/product/components/qna/createComment.vue";
import ViewComment from "@/components/product/components/qna/viewComment.vue";

import Cart from "@/components/cart/cart.vue";

import Order from "@/components/order/order.vue";
import OrderdList from "@/components/order/orderdList.vue";

import ManageProductList from "@/components/productManage/manageProductList.vue";
import ModifyProduct from "@/components/productManage/component/modifyProduct.vue";
import AddProduct from "@/components/productManage/component/addProduct.vue";

import ManageOrderList from "@/components/orderManage/manageOrderList.vue";

// store
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "PageHome",
    component: PageHome,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      isGuestPage: true,
      isManagePage: false,
    },
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      isGuestPage: true,
      isManagePage: false,
    },
  },
  {
    path: "/changeUserInfo",
    name: "ChangeUserInfo",
    component: ChangeUserInfo,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },
  {
    path: "/product/:productId",
    name: "Product",
    component: Product,
    meta: {
      isGuestPage: true,
      isManagePage: false,
    },
  },
  {
    path: "/cart",
    name: "Cart",
    component: Cart,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },
  {
    path: "/order",
    name: "order",
    component: Order,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },
  {
    path: "/orderdList",
    name: "orderdList",
    component: OrderdList,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },
  {
    path: "/manageProduct",
    name: "manageProduct",
    component: ManageProductList,
    meta: {
      isGuestPage: false,
      isManagePage: true,
    },
  },
  {
    path: "/manageOrder",
    name: "manageOrder",
    component: ManageOrderList,
    meta: {
      isGuestPage: false,
      isManagePage: true,
    },
  },
  {
    path: "/manageProduct/:productId",
    name: "modifyProduct",
    component: ModifyProduct,
    meta: {
      isGuestPage: false,
      isManagePage: true,
    },
  },
  {
    path: "/addProduct",
    name: "addProduct",
    component: AddProduct,
    meta: {
      isGuestPage: false,
      isManagePage: true,
    },
  },
  {
    path: "/createComment",
    name: "CreateComment",
    component: CreateComment,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },
  {
    path: "/viewComment/:commentId",
    name: "ViewComment",
    component: ViewComment,
    meta: {
      isGuestPage: false,
      isManagePage: false,
    },
  },

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = store.getters["authState/isLoggedIn"];
  const userData = store.getters["authState/userData"];
  if (to.matched.some((record) => record.meta.isGuestPage)) {
    if (isLoggedIn) {
      const pageName = to.name;
      console.log(pageName);
      if (pageName == "Product") {
        next();
        return;
      }
      next({ name: "PageHome" });
    } else {
      next();
    }
  } else {
    next();
  }
  if (to.matched.some((record) => record.meta.isManagePage)) {
    if (userData.userKubunId == 2) {
      next();
    } else {
      next({ name: "PageHome" });
    }
    next();
  }
});

export default router;
