import Vue from "vue";
import Vuex from "vuex";

import createPersistedState from "vuex-persistedstate";

import authState from "@/store/modules/auth";
import productState from "@/store/modules/product";
import cartState from "@/store/modules/cart";
import orderState from "@/store/modules/order";
import productManageState from "@/store/modules/productManage";
import qnaState from "@/store/modules/qna";
import orderManageState from "@/store/modules/orderManage";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authState,
    productState,
    cartState,
    orderState,
    productManageState,
    qnaState,
    orderManageState,
  },
  plugins: [createPersistedState()],
});
