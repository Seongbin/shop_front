import actions from "@/store/modules/order/order.actions.js";
import getters from "@/store/modules/order/order.getters.js";
import mutations from "@/store/modules/order/order.mutations.js";

const state = () => {
  return {
    readyToOrders: null,
    orders: null,
  };
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
