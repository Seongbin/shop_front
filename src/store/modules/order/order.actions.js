import axiosConfig from "@/config/axiosConfig.js";
import * as types from "@/store/modules/order/order.types.js";

import router from "@/router";

export default {
  async getOrders({ commit }) {
    commit(types.GET_ORDER_PENDING);
    try {
      const orders = await axiosConfig.get("/orders");
      const payload = {
        orders: orders.data,
      };
      commit(types.GET_ORDER_SUCCESS, payload);
    } catch (err) {
      commit(types.GET_ORDER_FAIL);
    }
  },

  async readyToOrders({ commit }, payload) {
    commit(types.READY_ORDER_PENDING);
    try {
      commit(types.READY_ORDER_SUCCESS, payload);
      router.push({ name: "order" });
    } catch (err) {
      commit(types.READY_ORDER_FAIL);
    }
  },

  async readyToOrdersDelete({ commit }, payload) {
    commit(types.READY_ORDER_DELETE_PENDING);
    try {
      commit(types.READY_ORDER_DELETE_SUCCESS, payload);
    } catch (err) {
      commit(types.READY_ORDER_DELETE_FAIL);
    }
  },

  async order({ commit, dispatch }, payload) {
    commit(types.ORDER_PENDING);
    try {
      await axiosConfig.post("/order", payload);
      commit(types.ORDER_SUCCESS, payload);
      const cartIds = [];
      payload.ordersDto.forEach((element) => {
        cartIds.push(element.id);
      });
      payload = {
        cartIds,
      };

      dispatch("cartState/deleteCarts", payload, { root: true });
      router.push({ name: "orderdList" });
    } catch (err) {
      commit(types.ORDER_FAIL);
    }
  },

  async cancelOrder({ commit }, formData) {
    commit(types.CANCEL_ORDER_PENDING);
    try {
      await axiosConfig.post("/cancelOrder", formData);
      commit(types.CANCEL_ORDER_SUCCESS, formData);
    } catch (err) {
      commit(types.CANCEL_ORDER_FAIL);
    }
  }
};
