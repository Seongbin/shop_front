import * as types from "@/store/modules/order/order.types.js";

export default {
  [types.GET_ORDER_SUCCESS](state, payload) {
    state.orders = payload.orders;
  },
  [types.READY_ORDER_SUCCESS](state, payload) {
    state.readyToOrders = payload;
  },
  [types.READY_ORDER_DELETE_SUCCESS](state, payload) {
    console.log("33");
    console.log(payload.readyToOrderIds);
    console.log(state.readyToOrders);
    payload.readyToOrderIds.forEach((selectedElement) => {
      let index = 0;
      state.readyToOrders.forEach((readyToOrdersElement) => {
        if (selectedElement == readyToOrdersElement.id) {
          console.log("selectedElement.id " + selectedElement);
          console.log("readyToOrdersElement.id" + readyToOrdersElement.id);
          state.readyToOrders.splice(index, 1);
        }
        index++;
      });
    });
  },
  [types.ORDERD_LIST_SUCCESS](state, payload) {
    console.log(payload);
    state.orders = payload.orders;
  },
  [types.CANCEL_ORDER_SUCCESS](state, payload) {
    console.log("33");
    console.log(payload.orderids);
    console.log(state.orders);
    payload.orderIds.forEach((selectedElement) => {
      let index = 0;
      state.orders.forEach((ordersId) => {
        if (selectedElement == ordersId.id) {
          state.orders.splice(index, 1);
        }
        index++;
      });
    });
  },
};