import * as types from "@/store/modules/qna/qna.types.js";

import axiosConfig from "@/config/axiosConfig.js";

import router from "@/router";

export default {
  async getQnas({ commit }, productId) {
    console.log(productId)
    commit(types.GET_QNAS_PENDING);
    try {
      const qna = await axiosConfig.get("/qnas/" + productId);
      console.log(qna.data);
      commit(types.GET_QNAS_SUCCESS, qna.data);
    } catch (err) {
      commit(types.GET_QNAS_FAIL);
    }
  },

  async getQna({ commit, dispatch }, qnaId) {
    commit(types.GET_QNA_PENDING);
    try {
      const qna = await axiosConfig.get("/qna/" + qnaId);
      commit(types.GET_QNA_SUCCESS, qna.data);

      dispatch('getComment', {
        id: qnaId
      });

      router.push({
        name: 'ViewComment',
        params: {
          commentId: qna.id
        }
      });
    } catch (err) {
      commit(types.GET_QNA_FAIL);
    }
  },

  async postQna({ commit }, formData) {
    console.log(formData)
    commit(types.POST_QNA_PENDING);
    try {
      await axiosConfig.post("/qna", formData);
      commit(types.POST_QNA_SUCCESS);
      router.push({
        name: 'Product',
        params: { productId: formData.productId }
      });
    } catch (err) {
      commit(types.POST_QNA_FAIL);
    }
  },

  async getQnaReply({ commit }) {
    commit(types.GET_QNA_REPLY_PENDING);
    try {
      await axiosConfig.get("/qnaReply");
      commit(types.GET_QNA_REPLY_SUCCESS);

    } catch (err) {
      commit(types.GET_QNA_REPLY_FAIL);
    }
  },

  async postQnaReply({ commit, dispatch }, formData) {
    commit(types.POST_QNA_REPLY_PENDING);
    try {
      console.log("###");
      console.log(formData.qnaBoardId);

      await axiosConfig.post("/qnaReply", formData);
      commit(types.POST_QNA_REPLY_SUCCESS);

      dispatch('getComment', {
        id: formData.qnaBoardId
      });

    } catch (err) {
      commit(types.POST_QNA_REPLY_FAIL);
    }
  },

  async getComment({ commit }, formData) {
    commit(types.GET_COMMENT_PENDING);
    try {

      const qnaReplays = await axiosConfig.get("/qnaReply/" + formData.id);
      console.log(qnaReplays);
      commit(types.GET_COMMENT_SUCCESS, qnaReplays.data);
    } catch (err) {
      commit(types.GET_COMMENT_FAIL);
    }
  }
}