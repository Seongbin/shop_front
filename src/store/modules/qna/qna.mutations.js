import * as types from "@/store/modules/qna/qna.types.js";

export default {
  [types.GET_QNAS_SUCCESS](state, qnas) {
    state.qnas = qnas;
  },

  [types.GET_QNA_SUCCESS](state, qna) {
    state.qna = qna;
  },

  [types.GET_COMMENT_SUCCESS](state, qnaReplys) {
    state.qnaReplys = qnaReplys;
  }
}