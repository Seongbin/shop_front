export default {
  qnas: (state) => state.qnas,
  qna: (state) => state.qna,
  qnaReplys: (state) => state.qnaReplys,
}