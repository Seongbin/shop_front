import actions from "@/store/modules/qna/qna.actions.js";
import getters from "@/store/modules/qna/qna.getters.js";
import mutations from "@/store/modules/qna/qna.mutations.js";

const state = () => {
  return {
  };
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
