import axiosConfig from "@/config/axiosConfig.js";
import * as types from "@/store/modules/auth/auth.types.js";

import router from "@/router";

export default {
  async login({ commit, dispatch }, formData) {
    commit(types.LOGIN_USER_PENDING);

    try {
      const loginResult = await axiosConfig.post("/login", formData);
      const payload = {
        jwt: loginResult.data,
      };
      commit(types.LOGIN_USER_SUCCESS, payload);
      dispatch("loadUser");
    } catch (err) {
      console.log(err);
      commit(types.LOGIN_USER_FAIL);
      dispatch("logout");
    }
  },

  async register({ commit }, formData) {
    try {
      commit(types.REGISTER_USER_PENDING);
      await axiosConfig.post("/register", formData);
      commit(types.REGISTER_USER_SUCCESS);
      router.push({ name: "PageHome" }).catch((err) => {
        throw new Error(`Problem handling something: ${err}.`);
      });
    } catch (err) {
      console.log(err);
      commit(types.REGISTER_USER_FAIL);
    }
  },

  async loadUser({ commit }) {
    try {
      const token = sessionStorage.getItem("Authorization");
      commit(types.LOAD_USER_PENDING);
      if (!token) {
        commit(types.LOAD_USER_FAIL);
        return false;
      }

      const userInfo = await axiosConfig.post("/loadUser");

      const payload = {
        userInfo: userInfo.data,
      };

      commit(types.LOAD_USER_SUCCESS, payload);
      router.push({ name: "PageHome" });
    } catch (err) {
      commit(types.LOAD_USER_FAIL);
    }
  },
  
  async logout({ commit }) {
    commit(types.LOGOUT_USER_PENDING);
    try {
      commit(types.LOGOUT_USER_SUCCESS);
      router.push({ name: "PageHome" });
    } catch (err) {
      commit(types.LOGOUT_USER_FAIL);
    }
  },

  async getUserKubun({ commit }) {
    commit(types.USER_KUBUN_PENDING);
    try {
      const userKubun = await axiosConfig.get("/getUserKubun");
      commit(types.USER_KUBUN_SUCCESS, userKubun.data);
    } catch (err) {
      commit(types.USER_KUBUN_FAIL);
    }
  },
  async changeUserInfo({ commit, dispatch }, formData) {
    commit(types.USER_CHANGE_PENDING);
    try {
      console.log(formData);
      await axiosConfig.post("/changeUserInfo", formData);
      dispatch("loadUser");
      commit(types.USER_CHANGE_SUCCESS);
    } catch (err) {
      commit(types.USER_CHANGE_FAIL);
    }
  },
};
