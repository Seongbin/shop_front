import * as types from "@/store/modules/auth/auth.types";

export default {
  [types.LOGIN_USER_SUCCESS](status, payload) {
    console.log(status);
    sessionStorage.setItem("Authorization", payload.jwt);
    console.log(sessionStorage.getItem("Authorization"));
  },

  [types.LOAD_USER_SUCCESS](status, payload) {
    status.userData = payload.userInfo;
  },

  [types.LOGOUT_USER_SUCCESS](status) {
    status.userData = null;
    sessionStorage.removeItem("Authorization");
  },

  [types.USER_KUBUN_SUCCESS](status, payload) {
    status.userKubun = payload;
  },
};
