export default {
  // user가 있을 때 로그인
  isLoggedIn: (state) => !!state.userData,
  userData: (state) => state.userData,
  userKubun: (state) => state.userKubun,
};
