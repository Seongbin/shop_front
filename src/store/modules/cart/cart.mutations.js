import * as types from "@/store/modules/cart/cart.types.js";

export default {
  [types.GET_CARTS_SUCCESS](state, payload) {
    state.carts = payload.carts;
  },

  [types.DELETE_CARTS_SUCCESS](state, payload) {
    payload.cartIds.forEach(selectedElement => {
      let index = 0;
      state.carts.forEach(cartsElement => {
        if (selectedElement == cartsElement.id) {
          state.carts.splice(index, 1);
        }
        index++;
      });
    });
  }
};
