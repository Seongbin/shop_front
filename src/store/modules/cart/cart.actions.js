import axiosConfig from "@/config/axiosConfig.js";
import * as types from "@/store/modules/cart/cart.types.js";

export default {
  async getCarts({ commit }) {
    commit(types.GET_CARTS_PENDING);
    try {
      const token = sessionStorage.getItem("Authorization");
      console.log(token);

      if (!token) {
        return false;
      }

      const carts = await axiosConfig.get("/carts");
      const payloads = {
        carts: carts.data,
      };
      commit(types.GET_CARTS_SUCCESS, payloads);

    } catch (err) {
      commit(types.GET_CARTS_FAIL);
    }
  },

  async addCart({ commit }, formData) {
    commit(types.ADD_CARTS_PENDING);
    try {
      await axiosConfig.post("/cart", formData);
      commit(types.ADD_CARTS_SUCCESS);
    } catch (err) {
      commit(types.ADD_CARTS_FAIL);
    }
  },

  async deleteCarts({ commit }, formData) {
    commit(types.DELETE_CARTS_PENDING);
    try {
      await axiosConfig.post("/deleteCarts", formData);
      commit(types.DELETE_CARTS_SUCCESS, formData);
    } catch (err) {
      commit(types.DELETE_CARTS_FAIL);
    }
  }
};
