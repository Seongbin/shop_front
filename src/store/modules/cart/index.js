import getters from "@/store/modules/cart/cart.getters.js";
import mutations from "@/store/modules/cart/cart.mutations.js";
import actions from "@/store/modules/cart/cart.actions.js";

const state = () => {
  return {
    carts: null,
  };
};
export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
