import * as types from "@/store/modules/orderManage/orderManager.types.js";

export default {
  [types.GET_ORDER_MANAGE_SUCCESS](state, orderManages) {
    state.orderManages = orderManages;
  },
}