import axiosConfig from "@/config/axiosConfig.js";
import * as types from "@/store/modules/orderManage/orderManager.types.js";

import router from "@/router";

export default {
  async getOrderManageList({ commit }) {
    commit(types.GET_ORDER_MANAGE_PENDING);
    try {
      const orderManages = await axiosConfig.get("/orderManages");
      commit(types.GET_ORDER_MANAGE_SUCCESS, orderManages.data);
      router.push(
        {
          name: "manageOrder"
        });
    } catch (err) {
      commit(types.GET_ORDER_MANAGE_FAIL);
    }
  },
  async postOrderState({ commit }, formData) {
    commit(types.POST_ORDER_MANAGE_PENDING);
    try {
      await axiosConfig.post("/orderState", formData);
      commit(types.POST_ORDER_MANAGE_SUCCESS);
    } catch (err) {
      commit(types.POST_ORDER_MANAGE_FAIL);
    }
  }
}
