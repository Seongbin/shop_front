import actions from "@/store/modules/orderManage/orderManage.actions.js";
import getters from "@/store/modules/orderManage/orderManage.getters.js";
import mutations from "@/store/modules/orderManage/orderManage.mutations.js";

const state = () => {
  return {
    product: null,
    products: null,
  };
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
