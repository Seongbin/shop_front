export default {
  productManageList: (state) => state.productManageList,
  productDto: (state) => state.productDto,
  productManageInfoDto: (state) => state.productManageInfoDto,
}