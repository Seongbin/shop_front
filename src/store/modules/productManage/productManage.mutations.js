import * as types from "@/store/modules/productManage/productManage.types.js";

export default {
  [types.GET_PRODUCT_LIST_SUCCESS](state, payload) {
    state.productManageList = payload;
  },
  [types.MODIFY_PRODUCT_GET_SUCCESS](state, payload) {
    state.productManageInfoDto = payload;
  },
  [types.MODIFY_PRODUCT_SUCCESS](state, payload) {
    state.productDto = payload;
  }
};
