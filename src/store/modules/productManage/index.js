import actions from "@/store/modules/productManage/productManage.actions.js";
import getters from "@/store/modules/productManage/productManage.getters.js";
import mutations from "@/store/modules/productManage/productManage.mutations.js";

const state = () => {
  return {
    readyToOrders: null,
    orders: null,
  };
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
