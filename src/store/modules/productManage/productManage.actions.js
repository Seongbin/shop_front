import axiosConfig from "@/config/axiosConfig.js";
import axiosPhotoConfig from "@/config/axiosPhotoConfig.js";
import axiosPhotoConfigNoContentType from "@/config/axiosPhotoConfigNoContentType.js";

import * as types from "@/store/modules/productManage/productManage.types.js";

import router from "@/router";

export default {
  async getProductManageList({ commit }) {
    commit(types.GET_PRODUCT_LIST_PENDING);
    try {
      const productManageList = await axiosConfig.get("/productManages");
      commit(types.GET_PRODUCT_LIST_SUCCESS, productManageList.data);
    } catch (err) {
      commit(types.GET_PRODUCT_LIST_FAIL);
    }
  },

  async addProduct({ commit, dispatch }, formData) {
    commit(types.ADD_PRODUCT_PENDING);
    try {
      const productId = await axiosConfig.post("/addProduct", formData);
      commit(types.ADD_PRODUCT_SUCCESS);
      dispatch("addPicture", {
        formData: formData,
        id: productId.data,
      });
      dispatch("addThumbnail", {
        formData: formData,
        id: productId.data,
      });
      router.push({ name: "manageProduct" });
    } catch (err) {
      commit(types.ADD_PRODUCT_FAIL);
    }
  },

  async addPicture({ commit }, payload) {
    commit(types.ADD_PICTURE_PENDING);
    const formData = new FormData();
    for (var index = 0; index < payload.formData.images.length; index++) {
      formData.append("fileList", payload.formData.images[index]);
    }
    formData.append("id", payload.id);

    try {
      await axiosPhotoConfig.post("/addPicture", formData);
      commit(types.ADD_PICTURE_SUCCESS);
    } catch (err) {
      commit(types.ADD_PICTURE_FAIL);
    }
  },

  async addThumbnail({ commit }, payload) {
    commit(types.ADD_THUMBNAIL_PENDING);
    const formData = new FormData();
    formData.append("file", payload.formData.thumbnail);
    formData.append("id", payload.id);
    try {
      await axiosPhotoConfig.post("/addThumbnail", formData);
      commit(types.ADD_THUMBNAIL_SUCCESS);
    } catch (err) {
      commit(types.ADD_THUMBNAIL_FAIL);
    }
  },

  async getProductInfo({ commit }, productId) {
    commit(types.MODIFY_PRODUCT_GET_PENDING);
    try {
      const productInfo = await axiosConfig.get(`/modifyProduct/` + productId);
      commit(types.MODIFY_PRODUCT_GET_SUCCESS, productInfo.data);
      router.push({
        name: "modifyProduct",
        params: { productId: productId },
      });
    } catch (err) {
      commit(types.MODIFY_PRODUCT_GET_FAIL);
    }
  },

  async modifyProductInfo({ commit, dispatch }, formData) {
    commit(types.MODIFY_PRODUCT_PENDING);
    try {
      const productInfo = await axiosConfig.post(
        `/modifyProduct`,
        formData
      );
      commit(types.MODIFY_PRODUCT_SUCCESS, productInfo.data);
      if (formData.newThumbNailFlag) {
        dispatch("modifyThumbnail", {
          formData,
          id: formData.id,
        });
      }
      if (formData.newPhotoFlag) {
        dispatch("deletePicture", {
          formData,
          id: formData.id,
        });
        dispatch("addPicture", {
          formData,
          id: formData.id,
        });
      }
      router.push({ name: "manageProduct" });
    } catch (err) {
      commit(types.MODIFY_PRODUCT_FAIL);
    }
  },

  async deleteProduct({ commit }, formData) {
    commit(types.DELETE_PRODUCT_PENDING);
    try {
      const requestData = {
        id: formData.id,
      };
      await axiosConfig.post("/deleteProduct", requestData);
      commit(types.DELETE_PRODUCT_SUCCESS);
      router.push({ name: "manageProduct" });
    } catch (err) {
      commit(types.DELETE_PRODUCT_FAIL);
    }
  },

  async deletePicture({ commit }, payload) {
    commit(types.DELETE_PICTURE_PENDING);
    const formData = {
      pictureDtoList: payload.formData.deletePictures,
    };
    try {
      await axiosPhotoConfigNoContentType.post("/deletePicture", formData);
      commit(types.DELETE_PICTURE_SUCCESS);
    } catch (err) {
      commit(types.DELETE_PICTURE_FAIL);
    }
  },

  async modifyThumbnail({ commit }, payload) {
    commit(types.MODIFY_THUMBNAIL_PENDING);
    const formData = new FormData();
    formData.append("file", payload.formData.thumbnail);
    formData.append("id", payload.id);
    try {
      await axiosPhotoConfigNoContentType.post("/modifyThumbnail", formData);
      commit(types.MODIFY_THUMBNAIL_SUCCESS);
    } catch (err) {
      commit(types.MODIFY_THUMBNAIL_FAIL);
    }
  },
};
