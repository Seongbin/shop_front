import axiosConfig from "@/config/axiosConfig.js";
import * as types from "@/store/modules/product/product.types.js";

export default {
  async getProducts({ commit }) {
    commit(types.PRODUCTS_FIRST_LOAD_PENDING);
    try {
      const products = await axiosConfig.get("/products");
      const payload = {
        products: products.data,
      };
      commit(types.PRODUCTS_FIRST_LOAD_SUCCESS, payload);
    } catch (err) {
      commit(types.PRODUCTS_FIRST_LOAD_FAIL);
    }
  },

  async getProduct({ commit }, productIdParam) {
    commit(types.PRODUCT_LOAD_PENDING);
    try {
      const product = await axiosConfig.get(
        "/product/" + productIdParam.productId
      );
      commit(types.PRODUCT_LOAD_SUCCESS, product.data);
    } catch (err) {
      commit(types.PRODUCT_LOAD_FAIL);
    }
  },
};
