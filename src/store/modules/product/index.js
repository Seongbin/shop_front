import actions from "@/store/modules/product/product.actions.js";
import getters from "@/store/modules/product/product.getters.js";
import mutations from "@/store/modules/product/product.mutations.js";

const state = () => {
  return {
    product: null,
    products: null,
  };
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
