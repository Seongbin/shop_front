import * as types from "@/store/modules/product/product.types.js";

export default {
  [types.PRODUCTS_FIRST_LOAD_SUCCESS](state, payload) {
    state.products = payload.products;
  },

  [types.PRODUCT_LOAD_SUCCESS](state, payload) {
    state.product = payload;
  },
};
